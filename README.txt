First of all, install realname and auto_nodetitle modules

==============================================================================
This an example of SQL query we use to test manually
==============================================================================
SELECT total.*
FROM
(SELECT r.realname AS node_title, r.uid AS nid, r.created AS node_created
FROM
realname r
WHERE  (r.uid <> '0')  UNION ALL SELECT node.title AS node_title, node.nid AS nid, node.created AS node_created
FROM
node node
ORDER BY node_created DESC) AS total WHERE total.node_title LIKE '%admin%'
LIMIT 10 OFFSET 0
