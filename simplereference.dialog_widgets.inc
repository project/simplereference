<?php

/**
 * @file
 * Implements hooks for references_dialog module.
 */


function simplereference_references_dialog_widgets() {
  return array(
    'simplereference_autocomplete' => array(
      'element_type' => 'textfield',
      'dialog_form' => 'simplereference_autocomplete_dialog_form',
      'views_query' => 'references_dialog_simplereference_views_query',
      'entity_type' => 'node',
      'format' => '$label',
      'operations' => array(
        'edit' => array(
          'function' => 'simplereference_simplereference_edit_link',
          'title' => t('Edit dialog'),
        ),
        'add' => array(
          'function' => 'simplereference_simplereference_add_link',
          'title' => t('Add dialog'),
        ),
        'search' => array(
          'function' => 'references_dialog_get_field_search_links',
          'title' => t('Search Dialog'),
        ),
      ),
    ),
  );
}

/**
 * Add link callback for node references.
 */
function simplereference_simplereference_add_link($element, $widget_settings, $field, $instance) {
  // Hide add link for non-empty default value.
  if (!empty($element['#default_value']) || !empty($element['#value'])) {
    return array();
  }
  $add_links = array();
  foreach ($instance['settings']['node_bundles'] as $type => $active) {
      $node_type = node_type_load($type);
      if (node_access('create', $node_type->type)) {
        $add_links[] = array(
          'title' => t('Create @type', array('@type' => $node_type->name)),
          'href' => 'node/add/' . strtr($type, array('_' => '-')),
        );
      }
  }
  foreach ($instance['settings']['taxonomy_term_bundles'] as $type => $active) {
      if (entity_access('create', 'taxonomy_term')) {
        $add_links[] = array(
          'title' => t('Create @type', array('@type' => $type)),
          'href' => 'admin/structure/taxonomy/' . $type . '/add'  ,
        );
      }
  }
  foreach ($instance['settings']['user_bundles'] as $type => $active) {
      if (entity_access('create', 'user')) {
        $add_links[] = array(
          'title' => t('Create @entity_user', array('@entity_user' => $type)),
          'href' => 'admin/people/create',
        );
      }
  }
  return $add_links;
}


/**
 * Edit link callback for node references.
 */
function simplereference_simplereference_edit_link($element, $widget_settings, $field, $instance) {
  if (isset($element['#default_value']) || isset($element['#value'])) {
    if (isset($element['#default_value'])) {
      $value = $element['#default_value'];
    } else {
      // Take "label [nid:id]', match the id from parenthesis.
      if (preg_match('/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\]$/', $element['#value'], $matches)) {
        list(, $title, $value) = $matches;
      } else {
        return array();
      }
    }

    $title = trim($value, '"');
    $result = _simplereference_sql_select_stored_value($title);
    if ($result) {
      while ($row = $result->fetchAssoc()) {
        $item['destination_entity_type'] = $row['entity_type'];
        $item['destination_entity_bundle'] = $row['entity_bundle'];
        $item['destination_entity_id'] = $row['entity_id'];
      }
    }

    if (entity_access('update', $item['destination_entity_type']) && isset($item)) {
      return array(
        simplereference_simplereference_link_helper($item['destination_entity_type'], $item['destination_entity_bundle'], $item['destination_entity_id'])
      );
    }
  }
  return array();
}

function simplereference_simplereference_link_helper($entity_type, $bundle, $entity_id) {
  $link = array();
  $link['title'] = t('Edit');
  if ($entity_type == 'user') {
    $link['href'] = 'user/' . $entity_id . '/edit';
  }
  elseif ($entity_type == 'taxonomy_term') {
    $link['href'] = 'taxonomy/term/' . $entity_id . '/edit';
  }
  elseif ($entity_type == 'node') {
    $link['href'] = 'node/' . $entity_id . '/edit';
  }
  else {
    $link['href'] = NULL;
  }
  return $link;
}


