<?php

/**
 * @file
 * This file contains a default view and code to overwrite its query and build a new one
 * This function build a new UNION ALL query
 * Some notes.
 * To modify query by default and join two tables: node and realnames see the following articles
 * https://drupal.org/node/748844
 * https://drupal.org/node/409808
 * http://sethsandler.com/code/drupal-6-creating-activity-stream-views-custom-sql-query-merging-multiple-views-part-1/
 * http://techatitsbest.com/blog/custom-views-3-queries-drupal-7
 * https://drupal.org/node/1848348
 * To bypass node access see the following snippet
 * http://drupal.stackexchange.com/questions/3927/how-to-bypass-node-access-when-using-entityfieldquery
 */

function simplereference_views_pre_execute(&$view) {
  if ($view->name == 'simplereference_references_dialog' && $view->current_display == 'references_dialog_1') {

    $query1 = $view->build_info['query'];
    $query1 ->addExpression(':nid', 'nid', array(':nid' => 'nid'));
    // define a virtual field
    $query1 ->addExpression(':entity_type_node', 'entity_type', array(':entity_type_node' => 'node'));

    // extracts my realnames
    $query2 = db_select('realname', 'r');

    // node_created and node_title are the same fields used by my_view query
    $query2->addField('r', 'realname', 'node_title');
    $query2->addField('r', 'created', 'node_created');
    $query2->addField('r', 'uid', 'nid');

    // define a virtual field
    $query2 ->addExpression(':entity_type_user', 'entity_type', array(':entity_type_user' => 'user'));

    //set some conditions if necessary
    $query2->condition('r.uid', '0', '<>');
    $query2->condition('r.realname', ',', '<>');

    //union custom query
    $query2->union($query1, 'UNION ALL');

    //extracts my taxonomy terms
    $query3 = db_select('taxonomy_term_data', 't');

    // node_created and node_title are the same fields used by my_view query
    $query3->addField('t', 'name', 'node_title');
    $query3->addField('t', 'tid', 'nid');
    // define a virtual field
    $query3 ->addExpression(':node_created', 'node_created', array(':node_created' => time()));
    $query3->addField('v', 'machine_name', 'entity_type');

    $query3->join('taxonomy_vocabulary','v','v.vid = t.vid');

    //set manually some conditions if necessary, for example, for vid = 1 use
    $query3->condition('v.vid','9');

    //union custom query with my_view default query
    $query3->union($query2, 'UNION ALL');

    //in order to prevent Cardinality violation errors i have to make a "total" query
    $total_query = db_select($query3, 'total')->fields('total');
    // to bypass node_access and avoid errors because this table is virtual and has not NIDs
    $total_query->addMetaData('account', user_load(1));
    // make where clause after generate UNION ALL
    // allows searching against node_title field
    if (isset($view->exposed_input['title'])) {
      $total_query->condition('total.node_title', '%' . $view->exposed_input['title'] . '%', 'LIKE');
    }
    //update views query and count_query with my new query.
    $view->build_info['query'] = $total_query;
    $view->build_info['count_query'] = $view->build_info['query']; //count_query is necessary for the pager

  }
}

function simplereference_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'simplereference_references_dialog';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'simplereference_references_dialog';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '21';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );

  /* Display: Reference dialog - Simplereference */
  $handler = $view->new_display('references_dialog', 'Reference dialog - Simplereference', 'references_dialog_1');
  $handler->display->display_options['display_description'] = 'For simplereferences';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['attach'] = array(
    0 => 'node:field_author:publication',
    1 => 'node:field_contributors:publication',
    2 => 'node:field_editor:publication',
  );

  /* Display: Reference dialog - Entity Refe */
  $handler = $view->new_display('references_dialog', 'Reference dialog - Entity Refe', 'references_dialog_2');
  $handler->display->display_options['display_description'] = 'For Entities References';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['attach'] = array(
    0 => 'node:field_news:news',
    1 => 'node:field_publications:news',
  );
  $translatables['simplereference_references_dialog'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Title'),
    t('Reference dialog - Simplereference'),
    t('For simplereferences'),
    t('Reference dialog - Entity Refe'),
    t('For Entities References'),
    t('Nid'),
  );
  $views['simplereference_references_dialog'] = $view;

  return $views;
}

